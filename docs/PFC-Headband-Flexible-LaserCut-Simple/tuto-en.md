# 3bis - Simpler flexible headband solution - a 70cm lanyard


This solution was inspired by a design by Dr. Carmen Ionescu, from the anaesthesia department of the Ixelles hospital, and shared under a [Creative Commons BY-NC-SA 4.0 license](https://creativecommons.org/licenses/by-nc-sa/4.0/). It is very fast to produce and less brittle than the rigid version.

**This version is an alternative of the [flexible headband to laser cutting](../PFC-Headband-Flexible-LaserCut/tuto-en.md).**

![](images/general.png)


# Hardware
* Hardware:
  * A sheet of transparent A4 PVC.
  * A sheet of Priplak® (polypropylene), 0.8mm thick, minimum length 70 cm.
* Tools:
  * Laser cutting machine
  * Cutter
* Production time: 1min30/piece


# Manufacturing process
## Step 1 - Cutting of the structure ("strip" below)


Download the template below (.SLDPRT or .DXF) and perform cutting.

The "bandelette-simple" band has been developed for any head dimensions, but is out of standard for 600 mm cutting machine.
The "bandelette-simple-small" band is for smaller cutting machine (600 mm), with a "SMALL" caption on it.

* [Bandelette-simple.SLDPRT](files/Bandeau-simple-masque-anti-projection-700-2.SLDPRT)
* [Bandelette-simple.dxf](files/Bandeau-simple-masque-anti-projection-700-2.DXF)
* [Bandelette-simple-small.SLDPRT](files/Bandeau-simple-masque-anti-projection-600.SLDPRT)
* [Bandelette-simple-small.dxf](files/Bandeau-simple-masque-anti-projection-600.DXF)

![](images/bandelette-simple.png)

Here is the nesting for cutting 25 bands in a 700mm x 510mm sheet :

![](images/25pcs.png)

* [SVG file](files/Bandeau-simple-masque-anti-projection-700-25pcs.svg)
* [DXF file](files/Bandeau-simple-masque-anti-projection-700-25pcs.dxf)

These are the settings for a 100W laser cutter with a 0.8mm polypropylene sheet :

* Speed : 2600 mm/s
* Power : 80 %

## Step 2 - Cut out the transparent A4 sheet of paper
Cut out 4 notches of 20mm long from the transparent sheet as shown in the drawing below (in mm). It has been found that an adjusted notch (with dimensions equal to the width of the strips) is necessary for a better hold of the transparent sheet.

![](images/cut1.png)


To simplify cutting and assembling, a version with 2 notches is also possible for sufficient support of the mask:


![](images/cut2.png)

It is very important to ensure that the size of the notch correspond perfectly to the size of the strip (a notch slightly smaller is preferred to a too large notch as the hole can be sligthly enlarged while inserting the strip).
It is this matching size that ensure the transparent sheet to fit correctly and stay in place. It also ensures a correct friction to adjust the spacing between the forehead of the user and the transparent sheet.

## Step 3 - Assembly
The assembly is done at the hospital, but is described for information:

* Insert the strip into the notches along the entire length of the sheet.

![](images/assembly1.png)

The visor is adjusted directly on the user's head. The visor is held away from the forehead by friction.


![](images/assembly2.jpg)

# Authors and Acknowledgements

Tutorial directed by Loïc Blanc and Ramzi Ben Hassen (BEAMS & Fablab ULB). Thanks to all the team for your contribution and your pictures.
