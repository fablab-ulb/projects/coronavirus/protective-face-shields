# 3ter - Flexible headband solution with cutter


This solution was inspired by a design by Dr. Carmen Ionescu, from the anaesthesia department of the Ixelles hospital, and shared under a [Creative Commons BY-NC-SA 4.0 license](https://creativecommons.org/licenses/by-nc-sa/4.0/).


**This is an alternative version of the [Flexible headband made with the laser cutter](../PFC-Headband-Flexible-LaserCut/tuto-en.md).**


![](files/headband-flexible-cutter-general.jpg)

It is very fast to produce and does not require any technological cutting machine.

## Hardware

* Hardware:
  * A sheet of transparent A4 PVC.
  * A sheet of Priplak® (polypropylene), 1mm thick, minimum length 70 cm.
* Tools:
  * Cutter or a pair of scissors
* Production time: 5min


##Manufacturing process

### Step 1 - Cutting of the structure ("strip" below)


Cut a long strip 20mm wide (can be adapted to 30mm) :



![](files/headband-flexible-dimensions.png)
![](files/headband-flexible-cutter-cut1.jpg)


Then, cut the T-shaped end to serve as a hook for the strip:


![](files/headband-flexible-cutter-cut2.png)

Make a hole for the notch. Measure your head circumference to place the hole in the right place. To make this hole, simply make a cross with the cutter.


![](files/headband-flexible-cutter-cut3.png)

Fold down both ends and cut them off with the cutter to prevent them from protruding.


![](files/headband-flexible-cutter-cut4.png)

Finally, cut out the initial "T" to make it easier to insert the hole into the notches in the plastic sheet:


![](files/headband-flexible-cutter-cut5.jpg)


### Step 2 - Cut the transparent A4 sheet of paper


Cut out 4 notches of 20mm long from the transparent sheet as shown in the drawing below (in mm). It has been found that an adjusted notch (with dimensions equal to the width of the strips) is necessary for a better hold of the transparent sheet.


![](files/headband-flexible-sheet-dimensions.png)

To simplify cutting and assembly, a version with 2 notches is also possible for a sufficient hold of the mask:


![](files/headband-flexible-sheet-dimensions2.png)

### Step 3 - Assembly
The assembly is done at the hospital, but is described for information:

1. Insert the strip into the notches along the entire length of the sheet.


![](files/headband-flexible-assembly1.png)

2. The hook on the back of the head is simply as follows:


![](files/headband-flexible-assembly2.png)

3. The adjustment of the visor is done directly on the user's head. The visor is held away from the forehead by friction.


![](files/headband-flexible-assembly3.png)

### Authors and Acknowledgements

Tutorial by Loïc Blanc (BEAMS & Fablab ULB). Thanks to all the team for your contribution and your pictures.
