# Solution charlotte

Une simple solution issue du service de Stomatologie et chirurgie maxillo-faciale du CHU St-Pierre à Bruxelles et du Fablab ULB ([licence Creative Commons BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr)).

![](../images/P4-Hygiene_cap.jpeg)

#### Matériel

* Matériel
  * Une Charlotte
  * Une feuille de plastique A4 transparente.
* Outils :
  * Agrafeuse
* Temps de production : 1 min/pièce

## Processus de fabrication

<figure class="video_container">
  <video controls="true" allowfullscreen="true" poster="../video_thumbnail.png">
    <source src="../video.mp4" type="video/mp4">
  </video>
</figure>

## Auteurs

Solution réalisée par

* Service de Stomatologie et chirurgie maxillo-faciale du CHU St-Pierre à Bruxelles :  
Aurélien Termont, Cyril Bouland, Antoine Yanni, Nadia Dahdouh, Thibaut Buset, Cynthia Watteeuw, Rokneddine Javadian, Didier Dequanter, Xavier Vanden Eynden, Caroline Gossiaux, Edward Boutremans, Medin Rey Susanna, Isabelle Loeb
* Fablab ULB: Victor Lévy
